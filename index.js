$(document).ready(function()
{
   $("a[data-rel='PhotoGallery1']").attr('rel', 'PhotoGallery1');
   $("#PhotoGallery1").magnificPopup({delegate:'a', type:'image', gallery: {enabled: true, navigateByImgClick: true}});
   $('#PhotoGallery1-filter a').click(function()
   {
      var value = $(this).data('filter-tag');
      if (value == 'all')
      {
         $('#PhotoGallery1 .thumbnail').show();
      }
      else
      {
         var filter = "[data-filter-tag='" + value + "']";
         $('#PhotoGallery1 .thumbnail').not(filter).hide();
         $('#PhotoGallery1 .thumbnail').filter(filter).show();
      }
      $('#PhotoGallery1-filter a').removeClass('active');
      $(this).addClass('active');
   });
   $("a[href*='#welcome']").click(function(event)
   {
      event.preventDefault();
      $('html, body').stop().animate({ scrollTop: $('#wb_welcome').offset().top-88 }, 600, 'easeOutCirc');
   });
});
